#!/vendor/bin/sh

PWMBASE=/sys/class/pwm/pwmchip1/pwm0

if [ ! -d $PWMBASE ]; then
    echo 0 > /sys/class/pwm/pwmchip1/export
fi
sleep 1
while [ ! -d $PWMBASE ]; do
    sleep 1
done

ISENABLE=`cat $PWMBASE/enable`
if [ $ISENABLE -eq 1 ]; then
    echo 0 > $PWMBASE/enable
fi
echo 50000 > $PWMBASE/period
echo 1 > $PWMBASE/enable

# max speed run 5s
echo 46990 > $PWMBASE/duty_cycle
sleep 5
echo 25000 > $PWMBASE/duty_cycle

# declare -a CpuTemps=(55000 43000 38000 32000)
# declare -a PwmDutyCycles=(1000 20000 30000 45000)

n=0
eval CpuTemps[$n]=75000
eval PwmDutyCycles[$n]=25000
eval Percents[$n]=100
n=1
eval CpuTemps[$n]=63000
eval PwmDutyCycles[$n]=35000
eval Percents[$n]=75
n=2
eval CpuTemps[$n]=58000
eval PwmDutyCycles[$n]=45000
eval Percents[$n]=50
n=3
eval CpuTemps[$n]=52000
eval PwmDutyCycles[$n]=46990
eval Percents[$n]=25

# turn on 2s
echo 46990 > $PWMBASE/duty_cycle
sleep 2

DefaultDuty=49990
DefaultPercents=0

while true;
do
    temp=$(cat /sys/class/thermal/thermal_zone0/temp)
    INDEX=0
    FOUNDTEMP=0
    DUTY=$DefaultDuty
    PERCENT=$DefaultPercents
    
    for i in 0 1 2 3; do
        if [ $temp -gt ${CpuTemps[$i]} ]; then
            INDEX=$i
            FOUNDTEMP=1
            break
        fi
    done
    if [ ${FOUNDTEMP} == 1 ]; then
        DUTY=${PwmDutyCycles[$i]}
        PERCENT=${Percents[$i]}
    fi

    echo $DUTY > $PWMBASE/duty_cycle;
    setprop vendor.pwmfan.current_speed ${PERCENT}

    # echo "temp: $temp, duty: $DUTY, ${PERCENT}%"
    # cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_cur_freq

    sleep 2s;
done

